#include <iostream>
#include <vector>
#include <cstdlib>

// Только для тестирования - в реализации не используется
#include <cassert>
#include <map>


// Шаблонный класс Декартова дерева, хранящего ключи типа Tkey,
// сравниваемые с помощью объекта less с типом Comparator
template <typename Tkey, typename Comparator>
class Treap {
    // Вершина дерева. Содержит ключ, указатели на левое и правое поддереево и приоритет,
    // по которому Декартово дерево является кучей.
    struct Node {
        Tkey key;
        int priority;
        Node *left;
        Node *right;
        
        Node() {
            priority = 0;
            left = right = NULL;
        };
    };
    
    Comparator less;
    Node *treapRoot;
    int numberOfNodes;
    
    // Делит дерево с корнем root на два: c ключами меньше и больше key соответственно
    void split (Node* root, const Tkey &key, Node* &left, Node* &right) {
        if (!root) {
            left = right = NULL;
            return;
        }
        if (less(key, root->key)) {
            split (root->left, key, left, root->left);
            right = root;
        } else {
            split (root->right, key, root->right, right);
            left = root;
        }
    };
    
    // Сливает два дерева в одно
    void merge(Node* &root, Node* left, Node* right) {
        if (!left) {
            root = right;
            return;
        }
        if (!right) {
            root = left;
            return;
        }
        if (left->priority > right->priority) {
            merge (left->right, left->right, right);
            root = left;
        } else {
            merge (right->left, left, right->left);
            root = right;
        }
    };
    
    // Рекурсивная процедура вставки вершины item в дерево
    void insert(Node* &root, Node* item) {
        if (!root) {
            root = item;
            return;
        }
        if (item->priority > root->priority) {
            split(root, item->key, item->left, item->right);
            root = item;
        } else {
            if (less(item->key, root->key)) {
                insert(root->left, item);
            } else {
                insert(root->right, item);
            }
        }
    };
    
    // Рекурсивная процедура извлечения вершины с ключом key из дерева
    Tkey extract(Node* &root, const Tkey &key) {
        if (!root) {
            return Tkey();
        }
        if (!(less(root->key, key) || less(key, root->key))) {
            Tkey result = root->key;
            delete root;
            --numberOfNodes;
            merge (root, root->left, root->right);
            return result;
        }
        if (less(key, root->key)) {
            return extract(root->left, key);
        } else {
            return extract(root->right, key);
        }
    };
    
    // Вычисление минимального значения в поддереве с корнем root
    Tkey findMin(Node* root) {
        if (!root) {
            return Tkey();
        }
        
        while (root->left) {
            root = root->left;
        }
        
        return root->key;
    };
    
    // Вычисление максимального значения в поддереве с корнем root
    Tkey findMax(Node* root) {
        if (!root) {
            return Tkey();
        }
        
        while (root->right) {
            root = root->right;
        }
        
        return root->key;
    };
    
    // Рекурсивная процедура проверки структуры (для тестирования)
    bool CheckStructure(Node* root) {
        if (!root) {
            return true;
        }
        
        if (root->left) {
            if (root->left->priority > root->priority || less(root->key, findMax(root->left))) {
                return false;
            }
        }
        
        if (root->right) {
            if (root->right->priority > root->priority || less(findMin(root->right), root->key)) {
                return false;
            }
        }
        
        return (CheckStructure(root->left) && CheckStructure(root->right));
    };
    
    // Рекурсивная процедура вычисления высоты (для тестирования)
    int height(Node* root) {
        if (!root) {
            return 0;
        }
        
        int leftHeight = height(root->left);
        int rightHeight = height(root->right);
        
        if (leftHeight > rightHeight) {
            return leftHeight + 1;
        } else {
            return rightHeight + 1;
        }
    };
    
public:
    Treap() {
        treapRoot = NULL;
        numberOfNodes = 0;
    };
    
    // Ищет, встречается ли ключ в дереве
    bool contains(const Tkey &key) {
        Node* root = treapRoot;
        while (root) {
            if (less(key, root->key)) {
                root = root->left;
            } else {
                if (less(root->key, key)) {
                    root = root->right;
                } else {
                    return true;
                }
            }
        }
        return false;
    };
    
    // Вставляет ключ в дерево
    void insert(const Tkey &key) {
        if (contains(key)) {
            return;
        }
        Node* item = new Node;
        ++numberOfNodes;
        item->key = key;
        item->priority = std::rand();
        insert(treapRoot, item);
    };
    
    // Извлекает ключ из дерева
    Tkey extract(const Tkey &key) {
        return extract(treapRoot, key);
    };
    
    // Возвращает максимальный ключ в дереве
    Tkey findMax () {
        return findMax(treapRoot);
    };
    
    // Процедура проверки структуры (для тестирования)
    bool CheckStructure() {
        return CheckStructure(treapRoot);
    }
    
    // Возвращает высоту дерева (для тестирования)
    int height() {
        return height(treapRoot);
    }
    
    // Возвращает число узлов в дереве
    int size() {
        return numberOfNodes;
    }
};

// Компаратор целых чисел - для тестирования
struct intComparator {
    bool operator () (int first, int second) const {
        return first < second;
    }
};

// Набор тестов для treap

class TreapTester {
    Treap<int, intComparator> treapForTesting;
    std::map<int, int> controlTree;
    
public:
    TreapTester() {};
    
    void testOneToTen() {
        treapForTesting = Treap<int, intComparator>();
        for (int i = 1; i < 11; ++i) {
            treapForTesting.insert(i);
            assert(treapForTesting.CheckStructure());
        }
        for (int i = 10; i > -1; --i) {
            int testValue = treapForTesting.extract(i);
            assert(testValue == i);
            assert(treapForTesting.CheckStructure());
        }
    };
    
    void testEmptyTreap() {
        treapForTesting = Treap<int, intComparator>();
        int testRequest = rand();
        assert(treapForTesting.extract(testRequest) == 0);
        testRequest = rand();
        assert(!treapForTesting.contains(testRequest));
    };
    
    void testOneElement() {
        treapForTesting = Treap<int, intComparator>();
        int element = rand();
        treapForTesting.insert(element);
        assert(treapForTesting.contains(element));
        assert(treapForTesting.extract(element) == element);
    };
    
    void testMaxInts() {
        treapForTesting = Treap<int, intComparator>();
        treapForTesting.insert(INT_MAX);
        treapForTesting.insert(INT_MIN);
        assert(treapForTesting.CheckStructure());
        assert(treapForTesting.contains(INT_MAX));
        assert(treapForTesting.contains(INT_MIN));
        assert(treapForTesting.extract(INT_MAX) == INT_MAX);
        assert(treapForTesting.extract(INT_MIN) == INT_MIN);
    };
    
    void testThousandRandoms() {
        treapForTesting = Treap<int, intComparator>();
        controlTree = std::map<int, int>();
        for (int i = 0; i < 1000; ++i) {
            int newKey = rand();
            treapForTesting.extract(newKey);
            treapForTesting.insert(newKey);
            controlTree[newKey] = newKey;
            assert(treapForTesting.CheckStructure());
        }
        
        for (int i = 0; i < 1000; ++i) {
            int keyToRemove = rand();
            treapForTesting.extract(keyToRemove);
            controlTree.erase(keyToRemove);
            assert(treapForTesting.CheckStructure());
        }
        
        for (int i = 0; i < 1000; ++i) {
            int request = rand();
            bool mapResult = (controlTree.find(request) != controlTree.end());
            bool treapResult = treapForTesting.contains(request);
            assert(mapResult == treapResult);
            assert(treapForTesting.CheckStructure());
        }
    };
    
    void testFindMax() {
        treapForTesting = Treap<int, intComparator>();
        controlTree = std::map<int, int>();
        for (int i = 0; i < 1000; ++i) {
            int newKey = rand();
            treapForTesting.extract(newKey);
            treapForTesting.insert(newKey);
            controlTree[-newKey] = -newKey;
            assert(treapForTesting.CheckStructure());
        }
        
        for (int i = 0; i < 1000; ++i) {
            int keyToRemoove = rand();
            treapForTesting.extract(keyToRemoove);
            controlTree.erase(-keyToRemoove);
            int mapResult = controlTree.begin()->second;
            int treapResult = treapForTesting.findMax();
            assert(mapResult == -treapResult);
            assert(treapForTesting.CheckStructure());
        }
    };
    
    void testMemoryLeak() {
        treapForTesting = Treap<int, intComparator>();
        
        for (int i = 1; i < 10001; ++i) {
            treapForTesting.insert(i);
            assert(treapForTesting.size() == i);
        }
        
        for (int i = 10000; i > -1; --i) {
            assert(treapForTesting.size() == i);
            treapForTesting.extract(i);
        }
        
        assert(treapForTesting.size() == 0);
    };
    
    int getHeightFromOneTo100000() {
        treapForTesting = Treap<int, intComparator>();
        
        for (int i = 1; i < 100001; ++i) {
            treapForTesting.insert(i);
        }
        
        return treapForTesting.height();
    };
    
    int getHeight100000Randoms() {
        treapForTesting = Treap<int, intComparator>();
        for (int i = 0; i < 100000; ++i) {
            int newKey = rand();
            treapForTesting.extract(newKey);
            treapForTesting.insert(newKey);
        }
        
        return treapForTesting.height();
    }
};


// Структура, описывающая блок памяти. Содержит его длину и начало
struct MemoryBlock {
    size_t length;
    size_t start;
    
    MemoryBlock (size_t newLength = 0, size_t newStart = 0) {
        length = newLength;
        start = newStart;
    }
};

// Реализует сравнение блоков по длине
struct lengthComparator {
    bool operator () (const MemoryBlock & first, const MemoryBlock & second) const {
        return (first.length < second.length) ||
        (first.length == second.length && first.start > second.start);
    }
};

// Реализует сравнение блоков по началу
struct startComparator {
    bool operator () (const MemoryBlock & first, const MemoryBlock & second) const {
        return (first.start < second.start);
    }
};

// Реализует сравнение блоков по концу
struct endComparator {
    bool operator () (const MemoryBlock & first, const  MemoryBlock & second) const {
        return (first.start + first.length < second.start + second.length);
    }
};

// Запрос к менеджеру памяти
struct MemoryRequest {
    int value;
    
    explicit MemoryRequest(int newValue) {
        value = newValue;
    }
    
    bool isProvideRequest() const {
        return value >= 0;
    }
    
    int sizeToProvide() const {
        return value;
    }
    
    int requestToClear() const {
        return -value - 1;
    }
};

// Необходимый вывод, соответсвующий не выполненному запросу на выделение памяти
const int LACK_OF_MEMORY = -1;

// Класс менеджера памяти
class MemoryManager {
    // Информация о выполненных запросах
    std::vector<MemoryBlock> doneRequests;
    // Три дерева со свободными блоками памяти
    Treap <MemoryBlock, lengthComparator> lengths;
    Treap <MemoryBlock, startComparator> begins;
    Treap <MemoryBlock, endComparator> ends;
    
public:
    MemoryManager() {};
    
    // Реализация запроса на выделение памяти
    // Возвращает выделенный блок
    // или MemoryBlock(0,0)
    // если выделение невозможно
    MemoryBlock provideMemory(size_t size) {
        
        MemoryBlock provided;
        MemoryBlock biggest = lengths.findMax();
        
        if (biggest.length > size) {
            provided.start = biggest.start;
            provided.length = size;
            
            MemoryBlock remain(biggest.length - size, biggest.start + size);
            
            lengths.extract(biggest);
            begins.extract(biggest);
            ends.extract(biggest);
            
            lengths.insert(remain);
            begins.insert(remain);
            ends.insert(remain);
        } else {
            if (biggest.length == size) {
                provided = biggest;
                lengths.extract(biggest);
                begins.extract(biggest);
                ends.extract(biggest);
            }
        }
        doneRequests.push_back(provided);
        return provided;
    };
    
    // Реализация запроса на освобождение памяти
    void clearMemmory (size_t requestNumber) {
        MemoryBlock empty;
        doneRequests.push_back(empty);
        
        MemoryBlock request = doneRequests[requestNumber];
        MemoryBlock right_neighbour (0, request.start + request.length);
        MemoryBlock left_neighbour (0, request.start);
        
        if (request.length == 0) {
            return;
        }
        
        if (begins.contains(right_neighbour)) {
            right_neighbour = begins.extract(right_neighbour);
            lengths.extract(right_neighbour);
            ends.extract(right_neighbour);
            request.length += right_neighbour.length;
        }
        
        if (ends.contains(left_neighbour)) {
            left_neighbour = ends.extract(left_neighbour);
            lengths.extract(left_neighbour);
            begins.extract(left_neighbour);
            request.length += left_neighbour.length;
            request.start -= left_neighbour.length;
        }
        
        lengths.insert(request);
        begins.insert(request);
        ends.insert(request);
        return;
    }
    
    // Симуляция работы с заданными параметрами и запросами и записью результата в output
    std::vector<int> workSimulation (const size_t memorySize,
                                     const size_t numberOfRequests,
                                     const std::vector<MemoryRequest>& requests) {
        
        std::vector<int> results;
        MemoryBlock freeBlock (memorySize, 0);
        lengths.insert(freeBlock);
        begins.insert(freeBlock);
        ends.insert(freeBlock);
        for (size_t requestNumber = 0; requestNumber < numberOfRequests; ++requestNumber) {
            if (requests[requestNumber].isProvideRequest()) {
                MemoryBlock result = provideMemory(requests[requestNumber].sizeToProvide());
                if (result.length == 0) {
                    results.push_back(LACK_OF_MEMORY);
                } else {
                    results.push_back(result.start + 1);
                }
            } else {
                clearMemmory(requests[requestNumber].requestToClear());
            }
        }
        return results;
    }
};

// Ввод  входных параметров
void scanInput(size_t * memorySize, size_t * numberOfRequests,
               std::vector<MemoryRequest> * requests, std::istream & inputStream = std::cin) {
    inputStream >> *memorySize;
    inputStream >> *numberOfRequests;
    
    for (size_t it = 0; it < *numberOfRequests; ++it) {
        int input;
        inputStream >> input;
        requests->push_back(MemoryRequest(input));
    }
};

// Вывод результата
void printOutput(const std::vector<int> & output, std::ostream & outputStream = std::cout) {
    for (int i = 0; i < output.size(); ++i) {
        outputStream << output[i] << std::endl;
    }
};

// Запуск набора тестов для treap
/*
 int main() {
 std::srand(42);
 TreapTester test;
 
 test.testOneToTen();
 test.testEmptyTreap();
 test.testOneElement();
 test.testMaxInts();
 test.testMemoryLeak();
 
 test.testThousandRandoms();
 test.testFindMax();
 
 std::cout << test.getHeightFromOneTo100000() << std::endl;
 // Мой результат - 39
 std::cout << test.getHeight100000Randoms() << std::endl;
 // Мой результат - 47
 return 0;
 }
 */

int main() {
    std::srand(42);
    size_t memorySize, numberOfRequests;
    std::vector<MemoryRequest> requests;
    
    scanInput(&memorySize, &numberOfRequests, &requests);
    
    MemoryManager manager;
    
    std::vector<int> output = manager.workSimulation(memorySize, numberOfRequests, requests);
    
    printOutput(output);
    
    return 0;
}
